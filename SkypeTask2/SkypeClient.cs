﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace SkypeTask2
{
    /// <summary>
    /// This class is written for a positive scenario
    /// That mean there is no check for network availability and etc.
    /// It was tested only in local Wi-Fi network
    /// </summary>
    class SkypeClient
    {
        // Event for sending for notifications connection estabilishment
        public delegate void ConnectionDelegate(bool isEstablished);
        public event ConnectionDelegate connectionEvent;

        // Event for sending for notifications about calling state
        public delegate void IncomingCommandDelegate(Commands command);
        public event IncomingCommandDelegate incomingCommandEvent;

        // If we start calling we create client object
        // In case if we receive call, will use client from listener
        private TcpClient tcpClient;

        // Listener can operate only with one client simultaneously
        private TcpListener tcpListener;

        public SkypeClient()
        {
            // Start listening incoming messages
            tcpListener = new TcpListener(SkypeUtility.GetInstance().ipAddress, SkypeUtility.GetInstance().standartPort);
            tcpListener.Start();
            ServerCycle();
        }
        private async void ServerCycle()
        {
            Byte[] bytes = new Byte[sizeof(Commands)];
            while (true)
            {
                tcpClient = await tcpListener.AcceptTcpClientAsync();
                NetworkStream stream = tcpClient.GetStream();

                int readedBytes;
                while ((readedBytes = await stream.ReadAsync(bytes, 0, bytes.Length)) == bytes.Length)
                {
                    Commands command = ConvertByteArrayToCommand(bytes);
                    incomingCommandEvent(command);
                    // Call cycle will finish when will receive STOP or BUSY
                    if (command == Commands.STOP_CALL || command == Commands.BUSY)
                        break;
                }
                CloseClient();
            }
        }

        #region Main operations
        public async void MakeCall(string destinationIP)
        {
            // We validate IP address before
            IPAddress ipAddress = IPAddress.Parse(destinationIP);
            try
            {
                tcpClient = new TcpClient();
                // Make connection
                await tcpClient.ConnectAsync(ipAddress, SkypeUtility.GetInstance().standartPort);
                // Notify that connection was successful
                connectionEvent(true);

                // send call request
                NetworkStream tcpStream = tcpClient.GetStream();
                byte [] bytes = ConvertCommandToByteArray(Commands.START_CALL);
                await tcpStream.WriteAsync(bytes, 0, bytes.Length);

                // Wait responce
                int readedBytes;
                while ((readedBytes = await tcpStream.ReadAsync(bytes, 0, bytes.Length)) == bytes.Length)
                {
                    // Notify application about incoming messages
                    Commands command = ConvertByteArrayToCommand(bytes);
                    incomingCommandEvent(command);

                    // Exit from cycle when received STOP or BUSY command
                    if (command == Commands.STOP_CALL || command == Commands.BUSY)
                    {
                        bytes = ConvertCommandToByteArray(command);
                        await tcpStream.WriteAsync(bytes, 0, bytes.Length);
                        break;
                    }
                }
            }
            catch(SocketException ex)
            {
                // Handle exception from ConnectAsync
                connectionEvent(false);
                CloseClient();
                Trace.WriteLine("Exception: " + ex.Message);
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Exception: " + ex.Message);
            }
        }
        public async Task HangUp()
        {
            // We should check existing connection
            if(tcpClient.Connected)
            {
                NetworkStream tcpStream = tcpClient.GetStream();
                await tcpStream.WriteAsync(ConvertCommandToByteArray(Commands.STOP_CALL), 0, sizeof(Commands));
            }
            else
            {
                // If case if no ready for use connection we will close TCP client
                CloseClient();
            }
        }
        public async Task Busy()
        {
            NetworkStream tcpStream = tcpClient.GetStream();
            await tcpStream.WriteAsync(ConvertCommandToByteArray(Commands.BUSY), 0, sizeof(Commands));
        }
        public async Task AnswerCall()
        {
            NetworkStream tcpStream = tcpClient.GetStream();
            await tcpStream.WriteAsync(ConvertCommandToByteArray(Commands.ANSWER_CALL), 0, sizeof(Commands));
        }
        #endregion

        #region Converters
        private byte[] ConvertCommandToByteArray(Commands command)
        {
            byte[] intBytes = BitConverter.GetBytes((int)command);
            return intBytes;
        }
        private Commands ConvertByteArrayToCommand(byte[] bytes)
        {
            Commands command = (Commands)BitConverter.ToInt32(bytes, 0);
            return command;
        }
        #endregion
        private void CloseClient()
        {
            if(tcpClient != null)
            {
                tcpClient.Close();
                tcpClient = null;
            }
        }
    }
}
