﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace SkypeTask2
{
    enum Commands { START_CALL = 1, ANSWER_CALL, STOP_CALL, BUSY };
    class SkypeUtility
    {
        // singletone instance
        private static SkypeUtility utility;

        public int standartPort { get; private set; }
        public IPAddress ipAddress { get; private set; }

        #region Initialize
        private SkypeUtility()
        {
            standartPort = 11000;
            GetIPAddress();
        }

        public static SkypeUtility GetInstance()
        {
            if (utility == null)
            {
                lock (typeof(SkypeUtility))
                {
                    if (utility == null)
                        utility = new SkypeUtility();
                }
            }
            return utility;
        }
        #endregion
        public void GetIPAddress()
        {
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface networkInterface in networkInterfaces)
            {
                IPInterfaceProperties properties = networkInterface.GetIPProperties();
                foreach (IPAddressInformation addressInformation in properties.UnicastAddresses)
                {
                    // Here I'm fetching network interfaces
                    // And select Wireless as default
                    // In case if you will use another type of connection change next condition to the right interface type
                    if (addressInformation.Address.AddressFamily == AddressFamily.InterNetwork && networkInterface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                    {
                        ipAddress = addressInformation.Address;
                    }
                }
            }
        }

        public bool IsValidIP(string ip)
        {
            IPAddress ipAddress;
            return IPAddress.TryParse(ip, out ipAddress);
        }
    }
}
