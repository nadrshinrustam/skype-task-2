﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkypeTask2
{
    public partial class MainWindow : Window
    {
        private const string STATUS_NOTHING         = "Ready for use";
        private const string STATUS_CONNECTING      = "Connecting";
        private const string STATUS_CALLING         = "Calling";
        private const string STATUS_CONVERSATION    = "Conversation";

        private SkypeClient client;
        public MainWindow()
        {
            InitializeComponent();
            client = new SkypeClient();
            client.connectionEvent += client_connectionEvent;
            client.incomingCommandEvent += client_incomingCommandEvent;

            myIPTextBlock.Text = SkypeUtility.GetInstance().ipAddress.ToString();
            ShowStartCallScreen();
        }

        void client_incomingCommandEvent(Commands command)
        {
            switch(command)
            {
                case Commands.START_CALL:
                    {
                        ShowAnswerCallScreen();
                        break;
                    }
                case Commands.STOP_CALL:
                    {
                        ShowStartCallScreen();
                        break;
                    }
                case Commands.ANSWER_CALL:
                    {
                        statusTextBlock.Text = STATUS_CONVERSATION;
                        ShowStopCallScreen();
                        break;
                    }
                case Commands.BUSY:
                    {
                        ShowStartCallScreen();
                        break;
                    }
            }
        }

        void client_connectionEvent(bool isEstablished)
        {
            if (isEstablished)
            {
                statusTextBlock.Text = STATUS_CALLING;
            }
            else
            {
                MessageBox.Show("Could not connect");
                ShowStartCallScreen();
            }
        }

        private void CallClick(object sender, RoutedEventArgs e)
        {
            if(SkypeUtility.GetInstance().IsValidIP(destinationIPTextBox.Text))
            {
                statusTextBlock.Text = STATUS_CONNECTING;
                ShowStopCallScreen();
                client.MakeCall(destinationIPTextBox.Text);
            }
            else
            {
                MessageBox.Show("Invalid ip address");
            }
        }
        private async void StopCallClick(object sender, RoutedEventArgs e)
        {
            StackPanelAction(stopCallPanel, false);
            await client.HangUp();
            StackPanelAction(stopCallPanel, true);
            ShowStartCallScreen();
        }

        private async void BusyClick(object sender, RoutedEventArgs e)
        {
            StackPanelAction(answerCallPanel, false);
            await client.Busy();
            StackPanelAction(answerCallPanel, true);
            ShowStartCallScreen();
        }

        private async void AnswerCall(object sender, RoutedEventArgs e)
        {
            StackPanelAction(answerCallPanel, false);
            await client.AnswerCall();
            StackPanelAction(answerCallPanel, true);
            statusTextBlock.Text = STATUS_CONVERSATION;
            ShowStopCallScreen();
        }

        private void ShowStartCallScreen()
        {
            statusTextBlock.Text = STATUS_NOTHING;
            startCallPanel.Visibility = System.Windows.Visibility.Visible;
            stopCallPanel.Visibility = System.Windows.Visibility.Collapsed;
            answerCallPanel.Visibility = System.Windows.Visibility.Collapsed;
        }
        private void ShowStopCallScreen()
        {
            startCallPanel.Visibility = System.Windows.Visibility.Collapsed;
            stopCallPanel.Visibility = System.Windows.Visibility.Visible;
            answerCallPanel.Visibility = System.Windows.Visibility.Collapsed;
        }
        private void ShowAnswerCallScreen()
        {
            statusTextBlock.Text = STATUS_CALLING;
            startCallPanel.Visibility = System.Windows.Visibility.Collapsed;
            stopCallPanel.Visibility = System.Windows.Visibility.Collapsed;
            answerCallPanel.Visibility = System.Windows.Visibility.Visible;
        }
        private void StackPanelAction(StackPanel panel, bool enable)
        {
            foreach(UIElement child in panel.Children)
            {
                child.IsEnabled = enable;
            }
        }
    }
}
